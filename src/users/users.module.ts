import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { PrismaModule } from 'prisma/prisma.module';
import { CloudinaryService } from './cloudinary.service';

@Module({
  controllers: [UsersController],
  imports: [PrismaModule],
  providers: [UsersService, JwtStrategy, CloudinaryService],
})
export class UsersModule {}
